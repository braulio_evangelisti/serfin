var gulp        = require('gulp'),
    sass        = require('gulp-sass'),               // scss a css
    browserSync = require('browser-sync').create(),   // actualiza navegado on save
    pkg         = require('./package.json');          // Este no se usa?
    critical    = require('critical'),                // genera index con css que se ve primero en head
    uglify      = require('gulp-uglify'),             // comprime js
    concat      = require('gulp-concat'),             // agrupa varios archivos en uno
    rename      = require('gulp-rename'),             // renombra archivos
    pump        = require('pump'),                    // maneja errores al concatenar tareas con .pipe()
    wait        = require('gulp-wait'),
    htmlmin     = require('gulp-htmlmin'),            // Comprime html
    jsSourceFiles  = 'src/js/**/*.js',                // fuente js
    cssSourceFiles = 'src/sass/**/*.scss',            // fuente sass
    phpSouceFiles  = ['./src/*.html','./src/*.php'];  // fuente php y html

/**
 * Dev task 
 * Watch for changes in any 
 * sass/*.scss, js/*.js or /*.html and /*.php files
 * & reloads browser
 */
gulp.task('dev', ['browserSync', 'sass', 'minjs', 'htmlmin'], function() {
  gulp.watch(cssSourceFiles,['sass']);
  gulp.watch(phpSouceFiles, ['htmlmin']);
  gulp.watch(jsSourceFiles, ['minjs']); 
  // gulp.watch('public/*.php', browserSync.reload);
  // gulp.watch('public/*.html', browserSync.reload);
});

/**
* SASS WATCHER
* Escucha cambios en archivos .scss dentro de sass y compila css/styles.css 
* Agregar objeto `{outputStyle: 'compressed'}` como parametro de sass() para generar styles.min.css 
* [sobreescribe styles.css actual, buscar como renombrarlo]
*/
gulp.task('sass', function(cb) {
  var cssDest = './public/css/';
  pump([
    gulp.src(cssSourceFiles),
    wait(500),
    sass(),
    gulp.dest(cssDest),
    rename('styles.min.css'),
    sass({outputStyle: 'compressed'}),
    gulp.dest(cssDest),
    browserSync.reload({
      stream: true
    })
  ],
    cb
  );
});

/**
 * minjs task
 * comprime codigo javascript dentro de /js en /public/js
 * 1 - concatena varios .js en uno desde la carpeta
 * 2 - duplica a .min.js
 * 3 - comprime con uglify()
 * 4 - recarga browser
 */
gulp.task('minjs', function (cb) {
  var jsDest = './public/js/';
  pump([
    gulp.src(jsSourceFiles),
    concat('scripts.js'),
    gulp.dest(jsDest),
    rename('scripts.min.js'),
    uglify(),
    gulp.dest(jsDest),
    browserSync.reload({
      stream: true
    })
  ],
    cb
  );
});

/**
 * htmlmin task
 * comprime codigo html de los archivos de la carpeta raiz
 */
gulp.task('htmlmin', function(cb){
  pump([
    gulp.src(phpSouceFiles),
    htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeEmptyAttributes: true,
      ignoreCustomFragments: [ /<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/ ],
      minifyCSS: true,
      minifyJS: true,
      minifyURLs: true
    }),
    gulp.dest('./public/'),
    browserSync.reload({
      stream: true
    })
  ],
    cb
  );
});

/**
 * browserSync TASK
 * inicializacion para sincronizar navegador con cambios
 */
gulp.task('browserSync', function() {
  browserSync.init({
      proxy: 'localhost/serfin/public/',
  });
});

/**
 * DEFAULT TASK CALL
 * ejecuta tarea `dev` como tarea por defecto al ejecutar `gulp`
 */
gulp.task('default', ['dev']); 

/***************************** 
* otras tareas [correr a mano] 
* ****************************/

/**
 * `gulp copy`
 * 
 * Copy vendor files from /node_modules into /vendor (para actualizar versiones de Bootstrap y jQuery)
 * NOTE: requires `npm install` before running!
 * Ejecutar cada vez que se actualicen bootstrap o jquery a traves de NPM
 */
gulp.task('copy', function() {
 
  gulp.src([
    'node_modules/bootstrap/dist/**/*',
    '!**/npm.js',
    '!**/bootstrap-theme.*',
    '!**/*.map'
  ])
  .pipe(gulp.dest('public/vendor/bootstrap'));

  gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
  .pipe(gulp.dest('public/vendor/jquery'));

});

/**
 * `gulp critical`
 * 
 * Genera el css "critico" para agregar al <head> que renderice primero
 * Ejecutar al final del desarrollo
 */
gulp.task('critical', ['dev'], function (cb) {
  critical.generate({
    inline: true,
    base: './public/',
    src: 'index.html',
    dest: 'index-critical.html',
    minify: true,
    width: 320,
    height: 480
  });
});