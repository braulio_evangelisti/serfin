$(document).ready(function() {
	var eDescripcion = $('#descripcion');
	
  if(eDescripcion[0] !== undefined){
		eDescripcion.summernote({
			tabsize: 2,
			height: 300,
			minHeight: 100,
			maxHeight: 500
		});

		$('.form').on('submit', function(e) {
		  if(eDescripcion.summernote('isEmpty')) {
		    alert('Por favor complete una descripción.');
		    // cancel submit
		    e.preventDefault();
		  } else { /*SUBMIT FORM*/ }
		})
	}
});

//Inicializa la tabla 


// var modalConfirm = function(callback){

//   $('#delete-modal').on('show.bs.modal', function (event) {
//     var button = $(event.relatedTarget) // Button that triggered the modal
//     var id = button.data('id'); // Extract info from data-* attributes
//     var name = button.data('name'); // Extract info from data-* attributes
//     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//     var modal = $(this);
//     //modal.find('.modal-title').text('New message to ' + recipient);
//     modal.find('.modal-body').text("Esta seguro que desea eliminar a " + nombre + "?");
//   })
  
//   // $("#btn-confirm").on("click", function(){
//   //   $("#mi-modal").modal('show');
//   // });

//   // $("#modal-btn-si").on("click", function(){
//   //   callback(true);
//   //   $("#mi-modal").modal('hide');
//   // });
  
//   // $("#modal-btn-no").on("click", function(){
//   //   callback(false);
//   //   $("#mi-modal").modal('hide');
//   // });

// };

// modalConfirm(function(confirm){
//   if(confirm){
//     //Acciones si el usuario confirma
//     $("#result").html("CONFIRMADO");
//   }else{
//     //Acciones si el usuario no confirma
//     $("#result").html("NO CONFIRMADO");
//   }
// });