<?php 
	if(isset($_GET['result']))
	    switch ($_GET['result']) {
	      case 'bad':
	          $result = "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fa fa-exclamation-triangle'></i> Ocurrió un error durante la operación.</div>";
	      break;
	      case 'ok':
	          $result = "<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><i class='fa fa-check-circle'></i> La operación se ha realizado con éxito.</div>";
	      break;
	    }