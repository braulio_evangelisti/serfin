<?php
      
  $urlubic = "";
  require($urlubic.'func.includes/seguridad.php');
  include_once($urlubic."func.includes/config.inc.php");
  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="keywords" content="<?=_global_metaKeys?>">
    <meta name="author" content="<?=_global_metaAuth?>">
    <link rel="icon" href="../img/favicon.ico">

    <title><?=_global_panelname?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
  </head>

  <body>

    <?php include('menu.php'); ?>

    <div class="container-fluid">
      <div class="row">

        <?php include('left_nav.php'); ?>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
          <!-- Main component for a primary marketing message or call to action -->
          <div class="jumbotron">
            <h1>Bienvenido, <?=$_SESSION['USERNAME']?></h1>
            <p>Este es el escritorio de su panel de control.</p>
            <p>Entre sus características principales se encuentran:</p>
            <p>
              <dl>
                <dt>Gestionar usuarios</dt>
                <dd><small>Gestión de los usuarios ingresados al sistema.</small></dd>
                <p></p>
                <dt>Administrar permisos</dt>
                <dd><small>Administración de permisos concedidos a usuarios según nivel de contenido.</small></dd>
                <p></p>  
                <dt>Actualizar contenido</dt>
                <dd><small>Crear y actualizar el contenido del sitio.</small></dd>
              </dl>
            </p>
              
            </p>
          </div>
        </main>
      </div>

    </div> <!-- /container -->
    
    <?php include('footer.php'); ?>
  </body>
</html>
