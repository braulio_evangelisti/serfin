<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
  
  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "administracion")!==false?'active':''?>" href="proceso.php?op=panel/administracion">
        <i class="fas fa-chart-bar"></i>
        Escritorio <span class="sr-only">(current)</span>
      </a>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link" href="#">Reports</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Analytics</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Export</a>
    </li> -->
  </ul>

  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "slide")!==false?'active':''?>" href="proceso.php?op=panel/slider">
        <i class="fas fa-image"></i>
        Slider
      </a>
    </li>
  </ul>

  <ul class="nav nav-pills flex-column">
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "empresa")!==false?'active':''?>" href="proceso.php?op=panel/empresa">
        <i class="fas fa-industry"></i>
        Empresa
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "servicio")!==false?'active':''?>" href="proceso.php?op=panel/servicios">
        <i class="fas fa-tasks"></i>
        Servicios
      </a>
    </li>
  </ul> 

  <ul class="nav nav-pills flex-column">
    <!-- <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "producto")!==false?'active':''?>" href="proceso.php?op=panel/productos">
        <i class="fas fa-shopping-basket"></i>
        Productos
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "noticia")!==false?'active':''?>" href="proceso.php?op=panel/novedades">
        <i class="far fa-newspaper"></i>
        Novedades
      </a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link <?=strpos($_SERVER['REQUEST_URI'], "categoria")!==false?'active':''?>" href="proceso.php?op=panel/categorias">
        <i class="fas fa-tags"></i>
        Categorias
      </a>
    </li>
  </ul>
</nav>