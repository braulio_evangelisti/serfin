<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <a class="navbar-brand" href="proceso.php?op=panel/administracion">Dashboard</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item active">
          <a class="nav-link" href="proceso.php?op=panel/administracion">Home <span class="sr-only">(current)</span></a>
        </li> -->

        <!-- <li class="nav-item"><a class="nav-link" href="proceso.php?op=panel/slider">Slider</a></li>
        
        <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Contenidos</a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" href="proceso.php?op=panel/novedades">Noticias</a>
            <a class="dropdown-item" href="proceso.php?op=panel/institucionales">Institucional</a>
            <a class="dropdown-item" href="proceso.php?op=panel/servicios">Servicios</a>
          </div>
        </li> 

        <li class="nav-item"><a class="nav-link" href="proceso.php?op=panel/categorias">Categorias</a></li>

        <li class="nav-item"><a class="nav-link" href="proceso.php?op=panel/productos">Productos</a></li> -->  
        
        <!-- <li class="nav-item">
          <a class="nav-link" href="proceso.php?op=panel/clientes">
            <i class="fas fa-briefcase"></i>
            Clientes
          </a>
        </li> -->
        
        <?php if($_SESSION['NIVEL'] == 0){ ?>
          <li class="nav-item">
            <a class="nav-link" href="proceso.php?op=panel/usuarios">
              <i class="fas fa-users"></i>
              Usuarios
            </a>
          </li>
        <?php } ?>

        <li class="nav-item">
          <a class="nav-link" href="<?=_global_siteurl?>" target="_blank"> 
            <i class="fas fa-globe"></i>
            Visitar sitio
          </a>
        </li>

        <!-- <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li> -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
                
        <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Bienvenido, <?=$_SESSION['USERNAME']?> 
            &nbsp; 
            <?php if($_SESSION['AVATAR'] != ''){ ?>
              <img src="<?=_global_userurl.$_SESSION['AVATAR']?>" class="hidden-xs" width="20" height="20">
            <?php } ?>
            <span style="display:none;" id="idOwner"><?=$_SESSION['id']?></span>
          </a>
          <div class="dropdown-menu" role="menu">
            <a href="proceso.php?op=edicion/de/usuarios&amp;id=<?=$_SESSION['id']?>" class="dropdown-item">
              <i class="fas fa-id-card"></i>
              Editar mi perfil
            </a>
            <a href="proceso.php?op=logout" title="Salir" class="dropdown-item">
              <i class="fas fa-sign-out-alt"></i>
              Cerrar sesión
            </a>
          </div>
        </li> 

      </ul>
      <!-- <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> -->
    </div>
  </nav>
</header>
<!-- <div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand label-info" href="proceso.php?op=panel/administracion" title="Pantalla de inicio"><i class="glyphicon glyphicon-home"></i></a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">

        <li><li><a href="proceso.php?op=panel/slider"><i class="glyphicon glyphicon-picture"></i> &nbsp;Slider</a></li></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th-large"></i> Contenidos
          <ul class="dropdown-menu" role="menu">
            <li><a href="proceso.php?op=panel/novedades">Noticias</a></li>
            <li><a href="proceso.php?op=panel/institucionales">Institucional</a></li>
            <li><a href="proceso.php?op=panel/servicios">Servicios</a></li>
          </ul>
        </li> 

        <li><a href="proceso.php?op=panel/categorias"><i class="glyphicon glyphicon-tags"></i> &nbsp;Categorias</a></li>

        <li><a href="proceso.php?op=panel/productos"><i class="glyphicon glyphicon-shopping-cart"></i> &nbsp;Productos</a></li>  

        <?php if($_SESSION['NIVEL'] == 0){ ?>
          <li><a href="proceso.php?op=panel/usuarios"><i class="glyphicon glyphicon-user"></i> &nbsp;Usuarios</a></li>
        <?php } ?>

        <li><a href="proceso.php?op=panel/clientes"><i class="glyphicon glyphicon-user"></i> &nbsp;Clientes</a></li>

        <li><a href="<?=_global_siteurl?>" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> Visitar sitio</a></li>

      </ul>

      <ul class="nav navbar-nav navbar-right">
                
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bienvenido, <?php echo $_SESSION['USERNAME']; ?> 
            &nbsp; 
            <?php if($_SESSION['AVATAR'] != ''){ ?>
              <img src="<?php echo _global_siteurl; ?>adm-gestor/frm_usuarios/<?php echo $_SESSION['AVATAR']; ?>" class="hidden-xs" width="20" height="20">
            <?php } ?>
            <span style="display:none;" id="idOwner"><?php echo $_SESSION['id']; ?></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="proceso.php?op=edicion/de/usuarios&amp;id=<?php echo $_SESSION['id']; ?>">Editar mi perfil</a></li>
            <li><a href="proceso.php?op=logout" title="Salir">Cerrar sesión</a></li>
          </ul>
        </li> 

      </ul>

    </div>
  </div>
</div> -->