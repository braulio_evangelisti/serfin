/* Igual a $(document).ready(function(){}); */
$(function(){
  /*video*/
  var $videoSrc;  
  $('.video-btn').click(function() {
      $videoSrc = $(this).data( "src" );
  });

  $('#myModal').on('shown.bs.modal', function (e) {
    $("#video-ss").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
  });

  $('#myModal').on('hide.bs.modal', function (e) {
      $("#video-ss").attr('src',$videoSrc); 
  });
  
  /* Contacto */
  // Get the form.
  var form = $('#ajax-contact');
  // Get the messages div.
  var formMessages = $('#form-messages');
  // Set up an event listener for the contact form.
  $(form).on('submit', function(event) {
    // Stop the browser from submitting the form.
    event.preventDefault();
    // Serialize the form data.
    var formData = $(form).serialize();
    // Submit the form using AJAX.
    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
    })
    .done(function(response) {
      // Make sure that the formMessages div has the 'success' class.
      $(formMessages).removeClass('alert-warning');
      $(formMessages).addClass('alert-success');

      // Set the message text.
      $(formMessages).html(response);

      // Clear the form.
      $('#nombre').val('');
      $('#email').val('');
      $('#asunto').val('');
      $('#mensaje').val('');
      grecaptcha.reset();
    })
    .fail(function(data) {
      // Make sure that the formMessages div has the 'error' class.
      $(formMessages).removeClass('alert-success');
      $(formMessages).addClass('alert-warning');
      // Set the message text.
      if (data.responseText !== '') {
          $(formMessages).html(data.responseText);
      } else {
          $(formMessages).text('Oops! Ha ocurrido un error y su mensaje no pudo ser enviado.');
      }
      grecaptcha.reset();
    });

  });
});

/**
 * Paralax slider
 * 
 */
var currentX = '';
var currentY = '';
var movementConstant = .025;

$(document).on('mousemove', function(e) {
  if(currentX == '') currentX = e.pageX;
  var xdiff = e.pageX - currentX;
  currentX = e.pageX;
  
  if(currentY == '') currentY = e.pageY;
  var ydiff = e.pageY - currentY;
  currentY = e.pageY; 
  
  $('.parallax div').each(function(i, el) {
    var movement = (i + 1) * (xdiff / (i-1) * movementConstant);
    var movementy = (i + 1) * (ydiff / (i-1) * movementConstant);
    var newX = $(el).position().left + movement;
    var newY = $(el).position().top + movementy;
    $(el).css('left', newX + 'px');
    $(el).css('top', newY + 'px');
  });
});

/**
 * Cambio fondo menu
 * 
 */
$(window).on('scroll', function() {    
  var scroll = $(window).scrollTop();

  if (scroll >= 20)
      $(".navbar").addClass("purple");
  else
      $(".navbar").removeClass("purple");
});
/**
 * Carga mapa contacto
 */
function initMap() {
  var latLng = {lat: -33.7492573, lng: -61.9644754};

  var mapOptions = {
      center: new google.maps.LatLng(latLng),
      zoom: 15,
      styles: [
        {
          "featureType": "all",
          "elementType": "labels.text.fill",
          "stylers": [
            {
                "saturation": "-20"
            },
            {
                "color": "#a28bb5"
            },
            {
                "lightness": "50"
            }
          ]
        },
        {
          "featureType": "all",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#a28bb5"
            },
            {
                "lightness": "-20"
            },
            {
                "saturation": "20"
            }
          ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "saturation": "15"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "10"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-40"
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-20"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-25"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-10"
                },
                {
                    "saturation": "20"
                }
            ]
        }
    ]
  }

  var map = new google.maps.Map(document.getElementById("map"), mapOptions);

  var contentString = '<div id="content">'+
    '<img src="./images/sanatorio-san-martin.jpg">'+
    '</div>'
    ;
  
  var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

  var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      title: 'Serfin Salud', 
      icon: 'https://www.clker.com/cliparts/c/9/m/4/B/d/google-maps-grey-marker-w-shadow-th.png'
  });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
}