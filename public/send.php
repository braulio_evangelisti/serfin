<?php

$urlubic = "panel/";
include_once($urlubic."func.includes/config.inc.php");
require($urlubic."func.includes/recaptchalib.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

   // Clean up the input values
   foreach($_POST as $key => $value) {
   	if(ini_get('magic_quotes_gpc'))
   		$_POST[$key] = stripslashes($_POST[$key]);

   	$_POST[$key] = htmlspecialchars(strip_tags($_POST[$key]));
   }

   /* NECESARIO RESPONSE CAPTCHA */
   $reCaptcha = new ReCaptcha(_captcha_privatekey);
   $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);

   if (empty($_POST["g-recaptcha-response"])){
      /* Si el captcha está vacío */
      header("HTTP/1.0 404 Not Found");
      echo "Captcha vacio";
      die();
   } else if(!$response->success){
      /* Si el captcha es incorrecto */
      header("HTTP/1.0 404 Not Found");
      echo "Verifique el campo Captcha";
      die();
   } else {  
      /* Si el captcha es correcto */

      // Assign the input values to variables for easy reference
      $nombre 	= $_POST["nombre"];
      $email   = $_POST["email"];
      $asunto  = $_POST["asunto"];
      $mensaje	= $_POST["mensaje"];

      // Test input values for errors
      $errors = [];
      if(strlen($nombre) < 2) {
      	if(!$nombre) {
      		$errors[] = "Debe ingresar un nombre";
      	} else {
      		$errors[] = "Deben ser más de 2 caracteres";
      	}
      }

      if(!$email) {
      	$errors[] = "Debe ingresar un email.";
      } else if(!validEmail($email)) {
      	$errors[] = "El email debe ser válido";
      }

      if(!$asunto)
         $errors[] = "Debe ingresar un asunto.";

      if(strlen($mensaje) < 10) {
      	if(!$mensaje) {
      		$errors[] = "Debe ingresar una consulta.";
      	} else {
      		$errors[] = "Ingrese al menos 10 caracteres.";
      	}
      }

   }

   if($errors) {
   	// Output errors and die with a failure message
      header("HTTP/1.0 404 Not Found"); 
   	$errortext = "";
   	foreach($errors as $error) {
   		$errortext .= "&middot; ". $error."<br>";
   	}
   	echo "Verifique los siguientes errores:<br>". $errortext;
      die();
   } else {

      require($urlubic.'func.includes/PHPMailer.php');
      require($urlubic.'func.includes/SMTP.php');

      $emailEnvio = "info@serfinsalud.com.ar";
      $emailTest  = "grafitodd@gmail.com";
      $consulta = "Nombre: ".$nombre."<br>Email: ".$email."<br>Consulta: ".$mensaje;

      $mail = new PHPMailer();
      $mail->From = "noreply@serfinsalud.com.ar";
      $mail->FromName = $nombre;
      $mail->Subject = "Nueva consulta de ".$nombre." recibida del sitio";
      $mail->AddAddress($emailEnvio);
      $mail->AddBCC($emailTest);
      $mail->WordWrap = 50;
      $mail->Body = $consulta;
      $mail->AltBody = $mensaje;
      $mail->Mailer = "smtp";
      $mail->Host = "polonez.websitewelcome.com";
      $mail->CharSet = 'UTF-8';
      $mail->IsHTML(true);
      $mail->SMTPAuth = true;
      $mail->Username = "noreply@serfinsalud.com.ar";
      $mail->Password = "4+b6]gWA1lZq";
      //$mail->SMTPSecure = 'tls';
      //$mail->Port = 465;

      if($mail->Send()) {
         echo "Gracias ! Hemos recibido su mensaje correctamente.<br>Le responderemos a la brevedad.";
      } else {
         header("HTTP/1.0 404 Not Found"); 
         $errorEnvio = $mail->ErrorInfo;
         echo "<span class='failure'>Error ENVIANDO CORREO: " . $errorEnvio . "</span>";
      }

   }

} else {
   // Not a POST request, set a 403 (forbidden) response code.
   http_response_code(403);
   echo "There was a problem with your submission, please try again.";
}
