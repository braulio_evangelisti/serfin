/**
 * Carga mapa contacto
 */
function initMap() {
  var latLng = {lat: -33.7492573, lng: -61.9644754};

  var mapOptions = {
      center: new google.maps.LatLng(latLng),
      zoom: 15,
      styles: [
        {
          "featureType": "all",
          "elementType": "labels.text.fill",
          "stylers": [
            {
                "saturation": "-20"
            },
            {
                "color": "#a28bb5"
            },
            {
                "lightness": "50"
            }
          ]
        },
        {
          "featureType": "all",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#a28bb5"
            },
            {
                "lightness": "-20"
            },
            {
                "saturation": "20"
            }
          ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "saturation": "15"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "10"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-40"
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-20"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-25"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-35"
                },
                {
                    "saturation": "20"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a28bb5"
                },
                {
                    "lightness": "-10"
                },
                {
                    "saturation": "20"
                }
            ]
        }
    ]
  }

  var map = new google.maps.Map(document.getElementById("map"), mapOptions);

  var contentString = '<div id="content">'+
    '<img src="./images/sanatorio-san-martin.jpg">'+
    '</div>'
    ;
  
  var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

  var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      title: 'Serfin Salud', 
      icon: 'https://www.clker.com/cliparts/c/9/m/4/B/d/google-maps-grey-marker-w-shadow-th.png'
  });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
}