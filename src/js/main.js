/* Igual a $(document).ready(function(){}); */
$(function(){
  /*video*/
  var $videoSrc;  
  $('.video-btn').click(function() {
      $videoSrc = $(this).data( "src" );
  });

  $('#myModal').on('shown.bs.modal', function (e) {
    $("#video-ss").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
  });

  $('#myModal').on('hide.bs.modal', function (e) {
      $("#video-ss").attr('src',$videoSrc); 
  });
  
  /* Contacto */
  // Get the form.
  var form = $('#ajax-contact');
  // Get the messages div.
  var formMessages = $('#form-messages');
  // Set up an event listener for the contact form.
  $(form).on('submit', function(event) {
    // Stop the browser from submitting the form.
    event.preventDefault();
    // Serialize the form data.
    var formData = $(form).serialize();
    // Submit the form using AJAX.
    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData
    })
    .done(function(response) {
      // Make sure that the formMessages div has the 'success' class.
      $(formMessages).removeClass('alert-warning');
      $(formMessages).addClass('alert-success');

      // Set the message text.
      $(formMessages).html(response);

      // Clear the form.
      $('#nombre').val('');
      $('#email').val('');
      $('#asunto').val('');
      $('#mensaje').val('');
      grecaptcha.reset();
    })
    .fail(function(data) {
      // Make sure that the formMessages div has the 'error' class.
      $(formMessages).removeClass('alert-success');
      $(formMessages).addClass('alert-warning');
      // Set the message text.
      if (data.responseText !== '') {
          $(formMessages).html(data.responseText);
      } else {
          $(formMessages).text('Oops! Ha ocurrido un error y su mensaje no pudo ser enviado.');
      }
      grecaptcha.reset();
    });

  });
});

/**
 * Paralax slider
 * 
 */
var currentX = '';
var currentY = '';
var movementConstant = .025;

$(document).on('mousemove', function(e) {
  if(currentX == '') currentX = e.pageX;
  var xdiff = e.pageX - currentX;
  currentX = e.pageX;
  
  if(currentY == '') currentY = e.pageY;
  var ydiff = e.pageY - currentY;
  currentY = e.pageY; 
  
  $('.parallax div').each(function(i, el) {
    var movement = (i + 1) * (xdiff / (i-1) * movementConstant);
    var movementy = (i + 1) * (ydiff / (i-1) * movementConstant);
    var newX = $(el).position().left + movement;
    var newY = $(el).position().top + movementy;
    $(el).css('left', newX + 'px');
    $(el).css('top', newY + 'px');
  });
});

/**
 * Cambio fondo menu
 * 
 */
$(window).on('scroll', function() {    
  var scroll = $(window).scrollTop();

  if (scroll >= 20)
      $(".navbar").addClass("purple");
  else
      $(".navbar").removeClass("purple");
});