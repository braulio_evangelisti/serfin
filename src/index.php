<?php 
  include_once("panel/func.includes/config.inc.php"); 
  $sCurrentPage = 'index';
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Instruct Internet Explorer to use its latest rendering engine -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=_global_metaTitle?></title>
    <meta name="keyword" content="<?=_global_metaKeys?>">
    <meta name="description" content="<?=_global_metaDesc?>">
    <meta name="author" content="<?=_global_metaAuth?>">

    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="apple-touch-icon" href="/custom-icon.png">

    <!-- Google font -->
    <style>
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,600i,700,700i,900,900i');
    </style>
    
    <!-- Helps prevent duplicate content issues -->
    <link rel="canonical" href="<?=_global_siteurl?>">

    <!-- Iconos -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/styles.min.css" rel="stylesheet">

    <!-- App config -->
    <link rel="manifest" href="manifest.json">
    <meta name="theme-color" content="#3b0549">
    
    <!-- Facebook OG -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?=_global_siteurl?>">
    <meta property="og:title" content="<?=_global_metaTitle?>">
    <meta property="og:image" content="<?=_global_siteurl?>images/serfin.svg">
    <meta property="og:description" content="<?=_global_metaDesc?>">
    <meta property="og:site_name" content="<?=_global_metaTitle?>">
    <meta property="og:locale" content="es_AR">
    
    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@<?=_social_twitter?>">
    <meta name="twitter:creator" content="@grafitodd">
    <meta name="twitter:url" content="<?=_global_siteurl?>">
    <meta name="twitter:title" content="<?=_global_metaTitle?>">
    <meta name="twitter:description" content="<?=_global_metaDesc?>">
    <meta name="twitter:image" content="<?=_global_siteurl?>images/serfin.svg">
  </head>

  <body id="body" data-spy="scroll" data-target="#navbarResponsive" data-offset="110" itemscope itemtype="http://schema.org/MedicalOrganization">

    <?php include 'nav.php'; ?>

    <header>  
      <div class="ese"></div>
        <?php 
          $aSlider = $oDB->slider()->where(['tipo' => 'header', 'publicada' => 'SI', 'eliminado' => 0])->order('orden ASC');
            if($aSlider){
        ?>
        <div id="carouselHeader" class="carousel slide" data-ride="carousel">
        
          <ol class="carousel-indicators">
            <?php 
              $iKey = 0;
              foreach ($aSlider as $aSlide) { 
            ?>
            <li data-target="#carouselHeader" data-slide-to="<?=$iKey?>" class="<?=$iKey==0?'active':''?>"></li>
            <?php 
                $iKey++;
              } 
            ?>
          </ol>
        
          <div class="carousel-inner" role="listbox">
      
        <?php 
          $iKey = 0;
          foreach ($aSlider as $aSlide) { 
        ?>
            <div class="carousel-item<?=$iKey==0?' active':''?>">
              <div class="carousel-caption d-none d-md-block">
                <h1><?=stripslashes($aSlide['titulo'])?></h1>       
                <p><?=stripslashes($aSlide['txtSlide1'])?></p>   
              </div>
             
              <div class="parallax">              
                <div class="water water-layer4"></div>
                <div class="water water-layer3"></div>
                <div class="water water-layer2" style="background-image: url(<?=_global_sliderurl.$aSlide['imagen']?>)"></div>
                <div class="water water-layer1"></div>
              </div>
            </div>
        <?php 
            $iKey++;
          } 
        ?>

          </div>
        
          <a href="#carouselHeader" class="carousel-control carousel-control-prev" role="button" data-slide="prev">
            <span aria-hidden="true"><img src="images/left.png"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a href="#carouselHeader" class="carousel-control carousel-control-next" role="button" data-slide="next">
            <span aria-hidden="true"><img src="images/right.png"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
      <?php } ?>
    </header>

    <?php 
      $aEmpresa = $oDB->empresa->where(['categoria_id' => 9, 'publicada' => 'SI', 'eliminado' => 0])->fetch();
      if($aEmpresa){
    ?>
    <!-- Page Content -->
    <section id="somos" class="somos py-5">
      <div class="container">
        <div class="col-12 text-center">
          <h1><?=$aEmpresa['titulo']?></h1>
          <legend><?=$aEmpresa['bajada']?></legend>
          <p><?=stripslashes($aEmpresa['descripcion'])?></p>
        </div>
      </div>
    </section>
    <?php } ?>

    <section id="about-us">
      <div class="container-full">
        <div class="about-us" style="background-image: url(<?=_global_insturl.$aEmpresa['imagen']?>)"></div>
      </div>
    </section>

    <?php 
      $aInstitucionales = $oDB->empresa->where(['categoria_id' => 1,'publicada' => 'SI', 'eliminado' => 0]);
      if($aInstitucionales){
    ?>
    <section id="valores" class="valores py-5">
      <div class="container">
        <div class="row">
          <?php 
            foreach ($aInstitucionales as $aInstitucional) { 
              if($aInstitucional->categoria['tipo']=='institucional'){
          ?>
            <div class="col-lg-4">
              <img class="mb-3" height="120px" src="<?=_global_insturl.$aInstitucional['imagen']?>" alt="<?=$aInstitucional['alt']?>" width="140" height="140">
              <h2><?=$aInstitucional['titulo']?></h2>
              <?=$aInstitucional['bajada']?>
              <?=stripslashes($aInstitucional['descripcion'])?>
            </div>
          <?php 
              } 
            }
          ?>
        </div>
      </div>
      
    </section>
    <?php } ?>
    
    <section id="espacio" class="espacio py-5">
      
        <div class="container">
          <div class="row">
            <!-- <div class="col-2"></div> -->
            <div class="col-12 text-center">
              <h1>Espacio físico y equipamiento</h1>
              <?php 
                $aSlider = $oDB->slider()->where(['tipo' => 'espacio', 'publicada' => 'SI', 'eliminado' => 0])->order('orden ASC');
                  if($aSlider){
                    $iKey = 0;
              ?>
              <div id="carouselEspacio" class="carousel slide p-5" data-ride="carousel">
                <!--  <ol class="carousel-indicators">
                  <li data-target="#carouselEspacio" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselEspacio" data-slide-to="1"></li>
                  <li data-target="#carouselEspacio" data-slide-to="2"></li>
                </ol> -->
                <div class="carousel-inner">
                  <?php foreach ($aSlider as $aSlide) { ?>
                  <div class="carousel-item <?=$iKey==0?'active':''?>">
                    <img class="d-block w-100" src="<?=_global_sliderurl.$aSlide['imagen']?>" alt="<?=stripslashes($aSlide['alt'])?>">
                  </div>
                  <?php 
                      $iKey++;
                    } 
                  ?>
                </div>
                
                <a class="carousel-control carousel-control-prev" href="#carouselEspacio" role="button" data-slide="prev">
                  <span class="" aria-hidden="true"><img src="images/left.png"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control carousel-control-next" href="#carouselEspacio" role="button" data-slide="next">
                  <span class="" aria-hidden="true"><img src="images/right.png"></span>
                  <span class="sr-only">Next</span>
                </a>

              </div>
              <?php } ?>
              <!-- <div class="col-1"></div> -->
          </div>
                          
          </div>
          
        </div>
      
    </section>
    <?php if($aCategorias){ ?>    
    <section id="servicios" class="servicios">
      <div class="container-full">
        <div class="row">
          <?php foreach ($aCategorias as $aCategoria) { ?>
          <div id="card-1" class="col-12 col-lg-3 card-ss" style="background: url(<?=_global_caturl.$aCategoria['imagen']?>) no-repeat top center">
             <div class="row">         
                <a href="servicio.php?slug=<?=$aCategoria['slug']?>">                 
                    <div class="card-frame"></div>
                    <div class="card-txt">
                      <h2><?=$aCategoria['nombre']?></h2>                      
                    </div>                  
                </a>               
              </div>     
          </div>
          <?php } ?>
        </div>
      </div>
    </section>
    <?php 
      }
    ?>
    <!-- <section id="video" class="video py-5 text-center">  
      <div class="container-full">
         
          <a class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/MQBeMLnkv5U" data-target="#myModal" href="#">
            <img src="images/play.svg" height="80px">
          </a>
  
            
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">

                  <div class="modal-body">

                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>        
                   
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="" id="video-ss"  allowscriptaccess="always">></iframe>
                    </div>
                    
                  </div>

                </div>
              </div>
            </div> 

      </div>    
    </section> -->

    <section id="contacto" class="contacto">
      <div class="container">
        <div class="row">
          <div class="form col-12 col-lg-6 py-4">
            <h1 class="py-2">Contacto</h1>
            <legend class="mb-4">Complete el formulario y nos comunicaremos con usted a la brevedad.</legend>
            <form id="ajax-contact" method="POST" action="send.php">
              <div id="form-messages" class="alert" role="alert"></div>
              <div class="form-group row">
                <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre y Apellido" autocomplete="name">
                </div>
              </div>
              <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="email">
                </div>
              </div>
              <div class="form-group row">
                <label for="asunto" class="col-sm-2 col-form-label">Asunto</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="asunto" id="asunto" placeholder="Motivo de consulta">
                </div>
              </div>
              <div class="form-group row">
                <label for="mensaje" class="col-sm-2 col-form-label">Mensaje</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="mensaje" id="mensaje" rows="4"></textarea>
                </div>
              </div>
              
              <div class="form-group row">
                <div class="col-sm-7 offset-sm-2">
                  <div class="g-recaptcha" data-sitekey="<?=_captcha_publickey?>"></div>
                </div>
                <div class="col-sm-3 text-right">
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </div>
            </form>

          </div>
          <div class="datos col-12 col-lg-6 py-4">
            <div class="datos-txt row text-center">
              <div class="col-12 col-lg-4">
                <span class="icon ion-ios-home" title="Direccion" aria-hidden="true"></span>
                <br>
                <p class="dato">Pellegrini 661 (Piso 4), 2600 Venado Tuerto, Santa Fe</p> <br>
               
              </div>
              <div class="col-12 col-lg-4">
                <span class="icon ion-ios-telephone" title="Telefono" aria-hidden="true"></span>
                <br>
                <p class="dato">(03462) 462990</p><br>
                
              </div>
              <div class="col-12 col-lg-4">
                <span class="icon ion-ios-email" title="Email" aria-hidden="true"></span> 
                <br>
                <p class="dato">info@serfinsalud.com.ar </p><br>
                
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </section>

    <section id="map" class="map">
      <div class="container-full">
        <div class="row">
          <div class="col-12 py-0">
            <!-- <div id="map"></div> -->
          </div>
        </div>
      </div>
    </section>

    <?php include 'footer.php'; ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script src="js/scripts.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDbbYInh8U3i3wcddd_tUj6BNREz-oYm-4&callback=initMap"></script>

  </body>

</html>
