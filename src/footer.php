<footer class="py-4 bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-12">
        <h2>Sobre nosotros</h2>
        <p>
          Servicio de rehabilitación del sanatorio San Martín.<br> 
        Un espacio joven en el cual se brinda una práctica jerarquizada en el ámbito de la prevención, promoción, tratamiento y rehabilitación de la salud. </p>
      </div>
      <div class="col-lg-4 col-12 text-center social">
        <h2>Seguinos</h2>
        <a href="https://facebook.com/<?=_social_facebook?>" target="_blank"><img height="40px" src="images/facebook-logo.svg"></a>
        <a href="https://instagram.com/<?=_social_instagram?>" target="_blank"><img height="40px" src="images/twitter.svg"></a>
      </div>
      <div class="menu-footer col-lg-4 col-12 text-right">
        <h2>Rehabilitación</h2>
        <?php 
          if($aCategorias){
        ?>
        <ul class="list-unstyled">
        <?php
            foreach ($aCategorias as $aCategoria) {
              $aName = explode(' ', $aCategoria['nombre']);
        ?>
        <li>
          <a href="servicio.php?slug=<?=$aCategoria['slug']?>" class="uppercase"><?=end($aName)?></a>
        </li>
        <?php 
            }
        ?>
        </ul>
        <?php 
          }
        ?>
      </div>
    </div>
  </div>
</footer>

<footer class="py-2 copyright">
  <div class="container">
    <p class="m-0 text-center">Copyright &copy; <?=_global_metaAuth . ' ' . date('Y')?></p>
  </div>
</footer>
